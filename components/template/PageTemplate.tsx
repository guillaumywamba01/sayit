import Head from "next/head"

export default function PageTemplate({ title }: { title: string }) {
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>

      <div className="h-[98vh] w-full flex items-center justify-center bg-colorLight dark:bg-colorLight_DM">
        <div>
          <p className="text-3xl text-center">{title}</p>

          <p className="mt-2 text-sm text-center">
            <span>Follow me on Linkedin : </span>
            <a
              className="text-colorPrimary font-bold"
              href="https://www.linkedin.com/in/guillaumy-wamba-405427180/"
              target="_blank"
              rel="noopener noreferrer">
              @Guillaumy-wamba
            </a>
          </p>

          <p className="mt-2 text-sm text-center">
            <a
              className="flex items-center justify-center gap-1 text-colorPrimary"
              href="https://www.linkedin.com/in/guillaumy-wamba-405427180/"
              target="_blank"
              rel="noopener noreferrer">
              <i className="bi bi-link text-2xl flex items-center justify-center"></i>
              <span>guillaumy-wamba</span>
            </a>
          </p>
        </div>
      </div>
    </>
  )
}
